//
//  DataService.swift
//  Fetching API Data
//
//  Created by Lam Trinh Tran Truc on 6/2/21.
//

import Foundation

class DataService {
    static let shared = DataService()
    fileprivate let baseURLString = "https://api.github.com"
    
    func fetchGists(completion: @escaping (Result<Any, Error>) -> Void) {
        var componentURL = URLComponents()
        componentURL.scheme = "https"
        componentURL.host   = "api.github.com"
        componentURL.path   = "/gists/public"
        
        guard let validURL = componentURL.url else {
            debugPrint("URL Creation failed..")
            return
        }
        
        URLSession.shared.dataTask(with: validURL) { data, response, error in
            if let httpResponse = response as? HTTPURLResponse {
                debugPrint("API Status: \(httpResponse.statusCode)")
            }
            
            guard let validData = data, error == nil else {
                completion(.failure(error!))
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: validData, options: [])
                completion(.success(json))
            } catch let serializationError {
                completion(.failure(serializationError))
            }
            
        }.resume()
    }
}
