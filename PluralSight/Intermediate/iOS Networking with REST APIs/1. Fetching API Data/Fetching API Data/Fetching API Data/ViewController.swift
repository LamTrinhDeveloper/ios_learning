//
//  ViewController.swift
//  Fetching API Data
//
//  Created by Lam Trinh Tran Truc on 6/2/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        DataService.shared.fetchGists { (result) in
            switch result {
            case .success(let json):
                debugPrint(json)
            case .failure(let error):
                debugPrint(error)
            }
        }
    }
    
}

